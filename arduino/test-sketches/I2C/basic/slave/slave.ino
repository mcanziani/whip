#include <Wire.h>

#define BLINK_TIME 100
#define SERIAL_BAUDRATE 9600

const byte bytDeviceAddress = 0x60;
byte bytRegisterAddress = 0x00;
bool flgLEDflash = false;

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  Wire.begin(bytDeviceAddress);  // join i2c bus with address
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent); // register event
  Serial.begin(SERIAL_BAUDRATE);  // start serial for output
}

void loop() {
      if (flgLEDflash) {
       digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW
       delay(BLINK_TIME);
       digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)
       delay(BLINK_TIME);
       flgLEDflash = false;
    }
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  while (Wire.available()) { // loop through all 
    byte c = Wire.read(); // receive byte
    Serial.print(c);         // print the byte
    flgLEDflash = true;
  }
  //int x = Wire.read();    // receive byte as an integer
  Serial.println("");         // print the integer
  
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Wire.write(0x60); // respond with message of 1 byte
  // as expected by master
}
