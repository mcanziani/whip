#include <Wire.h>

#define BLINK_TIME 100
#define BYTES_PER_REGISTER 1
#define SERIAL_BAUDRATE 9600

const byte bytDeviceAddress = 0x60;
byte bytFromSerial = 0x00;

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(SERIAL_BAUDRATE);  // start serial for output
  
}

byte x = 0;

void loop() {
  Wire.beginTransmission(bytDeviceAddress); // transmit to device
  Wire.write(x);              // sends one byte
  Wire.endTransmission();    // stop transmitting

  x++;
  delay(1500);

  Wire.requestFrom(bytDeviceAddress, BYTES_PER_REGISTER);    // request 6 bytes from slave device #8

  while (Wire.available()) { // slave may send less than requested
    byte c = Wire.read(); // receive a byte as character
    Serial.print(c);         // print the character
  }
  Serial.println("");         // print the integer
  delay(1500);
  
}
