#include <Wire.h>

#define BLINK_TIME 75
//#define SERIAL_BAUDRATE 9600

const byte bytDeviceAddress = 0x68;
byte bytRegisterAddress = 0x04;
byte bytRegisterWork = 0x00;
int flgLEDflash = 0;
//int flgState = 0; // 0 = WAITING, 1 = BEING READ, 2 = BEING WRITTEN TO

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  Wire.begin(bytDeviceAddress);  // join i2c bus with address
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent); // register event
//  Serial.begin(SERIAL_BAUDRATE);  // start serial for output
}

void loop() {
      if (flgLEDflash) {
       digitalWrite(LED_BUILTIN, HIGH);    // LED off
       delay(BLINK_TIME);
       digitalWrite(LED_BUILTIN, LOW);   // LED on
       delay(BLINK_TIME);
       flgLEDflash--;
    }
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
// the address byte is already checked by the Wire library
void receiveEvent(int byteCount) {
 
      if (byteCount == 1) {
        // 1 byte means a read operation start
        bytRegisterWork = Wire.read(); // receive register add byte and store it in the Work register
      } else if (byteCount > 1) {
        // 3 or more bytes means a write operation
        bytRegisterWork = Wire.read(); // receive register add byte and store it in the Work register
        int readRegisters = 0;
        while (Wire.available()) {
          byte c = Wire.read();
          readRegisters++;

          //register processing
          if (c==bytRegisterAddress) {
            flgLEDflash += 1;
          }
        }
      } else {
        // errors (byteCount <1) are ignored
      }

  /*
  while (Wire.available()) { // loop through all 
    byte c = Wire.read(); // receive byte
    Serial.print(c);         // print the byte
    flgLEDflash = true;
  }
  //int x = Wire.read();    // receive byte as an integer
  Serial.println("");         // print the integer
  */
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  if (bytRegisterWork==bytRegisterAddress){
    Wire.write(bytRegisterWork); // respond with message of 1 byte
    flgLEDflash += 2;
  } else {
    Wire.write(42); // respond with message of 1 byte
  }
  // as expected by master
}
