#include <Wire.h>
#include <SerialCommand.h>

#define BLINK_TIME 100
#define SERIAL_BAUDRATE 9600
#define DELAY_TIME 100

SerialCommand sCmd;
const byte bytDeviceAddress = 0x68;
byte bytFromSerial = 0x00;
byte bytWriteData = 0x04;
byte bytWriteAddress = 0x04;
int i2cRv = -1;
int cycle = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(SERIAL_BAUDRATE);  // start serial for output

  sCmd.addCommand("W", cmdWrite);          // Turns LED on
  sCmd.addCommand("R", cmdRead);         // Turns LED off
  sCmd.addCommand("T", cmdTest);        // Echos the string argument back
  sCmd.setDefaultHandler(cmdDefault);      // Handler for command that isn't matched
  Serial.println("Ready");
}

void loop() {
  sCmd.readSerial();
  /*
    if (Serial.available()) {
    // read the most recent byte
    bytFromSerial = Serial.read();


    if (bytFromSerial == 'r') {

        //bytWriteAddress = Serial.read();

        // Print operation Header
        Serial.print("Operation #");
        Serial.print(cycle++);
        Serial.print('\n');
        Serial.print("Operation\tDevice\tReg\tData\tACK");
        Serial.print('\n');

        // Read operation request
        i2cRv = i2cRequestRead(bytDeviceAddress, bytWriteAddress);
        Serial.print("Read \t \t 0x");
        Serial.print(bytDeviceAddress, HEX);
        Serial.print("\t 0x" );
        Serial.print(bytWriteAddress, HEX);
        Serial.print("\t\t ");
        Serial.print(i2cRv);
        Serial.print('\n');

        // Read operation
        while (Wire.available()) { // slave may send less than requested
          byte c = Wire.read(); // receive a byte as character
            Serial.print("Data \t \t 0x");
            Serial.print(bytDeviceAddress, HEX);
            Serial.print("\t \t 0x");
            Serial.print(c, HEX);
            Serial.print('\n');
        }

    } else if (bytFromSerial == 'w') {

        //bytWriteAddress = Serial.read();
        //bytWriteData = Serial.read();

        // Print operation Header
        Serial.print("Operation #");
        Serial.print(cycle++);
        Serial.print('\n');
        Serial.print("Operation\tDevice\tReg\tData\tACK");
        Serial.print('\n');

        // Write operation
        i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
        Serial.print("Write \t \t 0x");
        Serial.print(bytDeviceAddress, HEX);
        Serial.print("\t 0x");
        Serial.print(bytWriteAddress, HEX);
        Serial.print("\t 0x");
        Serial.print(bytWriteData, HEX);
        Serial.print("\t ");
        Serial.print(i2cRv);
        Serial.print('\n');

    }
    }
  */
}
int i2cWrite(byte deviceAddress, byte registerAddress, byte data) {
  // two registers = write
  Wire.beginTransmission(deviceAddress); // transmit to device
  Wire.write(registerAddress);
  Wire.write(data);              // sends one byte
  return Wire.endTransmission();    // stop transmitting
}
int i2cRequestRead(byte deviceAddress, byte registerAddress) {
  int rv;
  Wire.beginTransmission(deviceAddress); // transmit to device
  Wire.write(registerAddress);              // sends one byte
  rv = Wire.endTransmission(false);    // stop transmitting but don't release the bus
  Wire.requestFrom(deviceAddress, 1);    // request 1 byte from slave device #8
  return rv;
}

void cmdRead() {
  // retrieve arguments
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    bytWriteAddress = strtol(arg, NULL, 16);
  }
  else {
    Serial.println("Read command with no argument issued");
  }
  // Print operation Header
  Serial.print("Operation #");
  Serial.print(cycle++);
  Serial.print('\n');
  Serial.print("Operation\tDevice\tReg\tData\tACK");
  Serial.print('\n');

  // Read operation request
  i2cRv = i2cRequestRead(bytDeviceAddress, bytWriteAddress);
  Serial.print("Read \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x" );
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  // Read operation
  while (Wire.available()) { // slave may send less than requested
    byte c = Wire.read(); // receive a byte as character
    Serial.print("Data \t \t 0x");
    Serial.print(bytDeviceAddress, HEX);
    Serial.print("\t \t 0x");
    Serial.print(c, HEX);
    Serial.print('\n');
  }
}
void cmdWrite() {
  // retrieve arguments
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    bytWriteAddress = strtol(arg, NULL, 16);
    arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
    if (arg != NULL) {    // As long as it existed, take it
      bytWriteData = strtol(arg, NULL, 16);
    }
    else {
      Serial.println("Write command with no register data argument issued");
    }
  }
  else {
    Serial.println("Write command with no register address argument issued");
  }
  // Print operation Header
  Serial.print("Operation #");
  Serial.print(cycle++);
  Serial.print('\n');
  Serial.print("Operation\tDevice\tReg\tData\tACK");
  Serial.print('\n');

  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');
}
void cmdTest() {
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    Serial.print("Test, argument: ");
    Serial.println(strtol(arg, NULL, 16));
  }
  else {
    Serial.println("Test command with no argument issued");
  }
}
void cmdDefault() {
  Serial.println("Unrecognized command");
}
