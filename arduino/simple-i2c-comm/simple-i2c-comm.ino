#include <Wire.h>
#include <SerialCommand.h>

#define SERIAL_BAUDRATE 9600
#define DELAY_TIME 100

// I/O pin definitions
const int pinNRST = 2;
const int pinIO1 = 3;
const int pinLED = 8;
const int pinButton1 = 9;
const int pinButton2 = 10;
// Serial command library object
SerialCommand sCmd;
byte bytFromSerial = 0x00;
// Byte registers for I2C
byte bytDeviceAddress = 0x68;
byte bytWriteData = 0x04;
byte bytWriteAddress = 0x04;
// Auxiliary variables
int i2cRv = -1;
int cycle = 0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);  // initialize digital pin LED_BUILTIN as an output
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(SERIAL_BAUDRATE);  // start serial for output

  sCmd.addCommand("W", cmdWrite);       // Add Writing command
  sCmd.addCommand("R", cmdRead);        // Reading
  sCmd.addCommand("S", cmdSet);         // configure a device
  sCmd.addCommand("A", cmdAll);         // Turn on all LEDs on a device
  sCmd.addCommand("T", cmdTest);        // Test command
  sCmd.setDefaultHandler(cmdDefault);   // Handler for command that isn't matched

  // Set default values for pins
  pinMode(pinNRST, OUTPUT);
  digitalWrite(pinNRST, HIGH);

  pinMode(pinIO1, OUTPUT);
  digitalWrite(pinIO1, HIGH);

  pinMode(pinButton1, INPUT);
  pinMode(pinButton2, INPUT);

  pinMode(pinLED, OUTPUT);
  digitalWrite(pinLED, HIGH); // Turn ON LED on ArduinoShield

  Serial.println("Ready");
}

void loop() {
  sCmd.readSerial();
}

int i2cWrite(byte deviceAddress, byte registerAddress, byte data) {
  Wire.beginTransmission(deviceAddress);  // begin I2C transmission
  Wire.write(registerAddress);            // send the address of the register to be written
  Wire.write(data);                       // write the data
  return Wire.endTransmission();          // stop transmitting, return ACK value
}
int i2cRequestRead(byte deviceAddress, byte registerAddress) {
  int rv;
  Wire.beginTransmission(deviceAddress);  // begin I2C transmission
  Wire.write(registerAddress);            // send the address of the register to be read
  rv = Wire.endTransmission(false);       // stop transmitting but don't release the bus
  Wire.requestFrom(deviceAddress, 1);     // request 1 byte from slave device
  return rv;                              // return ACK value
}

void cmdSet() {
  // retrieve arguments
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    bytDeviceAddress = strtol(arg, NULL, 16);
  }
  else {
    Serial.println("Set command with incorrect argument count");
  }

  
  // Print operation Header
  Serial.print("Operation #");
  Serial.print(cycle++);
  Serial.print('\n');
  Serial.print("Operation\tDevice\tReg\tData\tACK");
  Serial.print('\n');

  bytWriteAddress = 0x00;
  bytWriteData = 0x01;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x14;
  bytWriteData = 0xFF;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');
  bytWriteAddress = 0x15;
  bytWriteData = 0xFF;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');
  bytWriteAddress = 0x16;
  bytWriteData = 0xFF;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');
  bytWriteAddress = 0x17;
  bytWriteData = 0xFF;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');
}

void cmdAll() {
  // retrieve arguments
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    bytDeviceAddress = strtol(arg, NULL, 16);
    arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
    if (arg != NULL) {    // As long as it existed, take it
      bytWriteData = strtol(arg, NULL, 16);
    } else {
      Serial.println("All command with incorrect argument count");
    }

  }
  else {
    Serial.println("All command with incorrect argument count");
  }

  
  // Print operation Header
  Serial.print("Operation #");
  Serial.print(cycle++);
  Serial.print('\n');
  Serial.print("Operation\tDevice\tReg\tData\tACK");
  Serial.print('\n');

  bytWriteAddress = 0x02;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x03;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

    bytWriteAddress = 0x04;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

    bytWriteAddress = 0x05;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x06;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x07;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x08;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x09;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x0A;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x0B;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x0C;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x0D;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x0E;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x0F;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x10;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  bytWriteAddress = 0x11;
  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

}

void cmdRead() {
  // retrieve arguments
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    bytDeviceAddress = strtol(arg, NULL, 16);

    arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
    if (arg != NULL) {    // As long as it existed, take it
      bytWriteAddress = strtol(arg, NULL, 16);
    } else {
      Serial.println("Read command with incorrect argument count");
    }
  }
  else {
    Serial.println("Read command with incorrect argument count");
  }
  // Print operation Header
  Serial.print("Operation #");
  Serial.print(cycle++);
  Serial.print('\n');
  Serial.print("Operation\tDevice\tReg\tData\tACK");
  Serial.print('\n');

  // Read operation request
  i2cRv = i2cRequestRead(bytDeviceAddress, bytWriteAddress);
  Serial.print("Read \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x" );
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t\t ");
  Serial.print(i2cRv);
  Serial.print('\n');

  // Read operation
  while (Wire.available()) { // slave may send less than requested
    byte c = Wire.read(); // receive a byte as character
    Serial.print("Data \t \t 0x");
    Serial.print(bytDeviceAddress, HEX);
    Serial.print("\t \t 0x");
    Serial.print(c, HEX);
    Serial.print('\n');
  }
}
void cmdWrite() {
  // retrieve arguments
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    bytDeviceAddress = strtol(arg, NULL, 16);
    arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
    if (arg != NULL) {    // As long as it existed, take it
      bytWriteAddress = strtol(arg, NULL, 16);
      arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
      if (arg != NULL) {    // As long as it existed, take it
        bytWriteData = strtol(arg, NULL, 16);
      }
      else {
        Serial.println("Write command with incorrect argument count");
      }
    } else {
      Serial.println("Write command with incorrect argument count");
    }

  }
  else {
    Serial.println("Write command with incorrect argument count");
  }
  // Print operation Header
  Serial.print("Operation #");
  Serial.print(cycle++);
  Serial.print('\n');
  Serial.print("Operation\tDevice\tReg\tData\tACK");
  Serial.print('\n');

  // Write operation
  i2cRv = i2cWrite(bytDeviceAddress, bytWriteAddress, bytWriteData);
  Serial.print("Write \t \t 0x");
  Serial.print(bytDeviceAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteAddress, HEX);
  Serial.print("\t 0x");
  Serial.print(bytWriteData, HEX);
  Serial.print("\t ");
  Serial.print(i2cRv);
  Serial.print('\n');
}
void cmdTest() {
  char *arg;
  arg = sCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    Serial.print("Test, argument: ");
    Serial.println(strtol(arg, NULL, 16));
  }
  else {
    Serial.println("Test command with no argument issued");
  }
}
void cmdDefault() {
  Serial.println("Unrecognized command");
}
