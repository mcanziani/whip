26.06.2017
COHMED meeting:
- Extensive discussion on the wound model and wounding technique
- At the end I showed the device prototype
- Prof. M�ller asked if the coating gives off smell, which would indicate something that could interfere with the cell cultures
- I replied that we do not know and we should test it, and suggested giving them a treated dummy PCB to test; they agreed

ITeM meeting:
- I reported that the hardware is (mostly) ready and that we should be able to test inside the incubator in two weeks.
- Prof M�ller asked if I had figured out how to "get in the incubator", it seems he is worried about the mechanical mount. I should discuss this further in the WH meeting, my approach right now is that I don't have time but I do not forsee any problems that I could fix better that anyone else
- The device paper for BMT has now been upgraded back from Paper to Talk

Internal WH meeting:
- We discussed about the previous meetings: multiple wounds, statistics, wounding technique, and mechanical mount
- Jacquelyn agrees with me about the mechanical mount

WH meeting:
- Jacquelyn described a proposed new wounding technique that came up during the day with a cylindrical shaped metal grid
- I showed the almost finished electronics and said that the main things remaining are programming, and the mechanical mount but now he did not seem so concerned
- I talked about how I showed Prof. M�ller the electronics and the concerns she raised about the coating emitting some chemicals, and my offer for a coated PCB dummy for testing
- I asked for a fume hood or some well-ventilated room for the coating and they offered one of the unused bathrooms on the M building but Ann-Kathrin also suggested we can contact a person in charge of the chemistry labs who can give us acces to a fume hood. She said she can contact this person and then let me know.
