function [ rv ] = integrateSpectra( x , y )
%INTEGRATE Summary of this function goes here
%   Detailed explanation goes here

assert(sum(size(x)~=size(y))==0, 'Function %s takes two vectors of equal size', ...
  mfilename);

rv=sum((x(2:length(x))-x(1:length(x)-1)).*y(1:length(y)-1));

end

