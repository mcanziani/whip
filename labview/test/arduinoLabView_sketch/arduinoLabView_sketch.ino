/**********************************************************************
 Arduino serial write and read with LabVIEW
 How to read a number from serial, do something in Arduino, 
 and write the result via Serial for LabVIEW
 author  Physics Light
 date    09 July 2014
 license Creative Commons 4.0 share alike 
************************************************************************/
#define BLINK_TIME 100
#define WAIT_TIME 500

long readNumber = 1;
void setup() {
  // init serial port baud rate
  Serial.begin(9600);
    // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  // check serial 
  if ( Serial.available() ){
    // cast the string read in an integer 
      readNumber = Serial.parseInt();

  }

      //write the result via Serial for LabVIEW
      Serial.println (readNumber);
      // turn on LED
      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(BLINK_TIME);                       // wait for a second
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(WAIT_TIME);
}
