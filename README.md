# README #

### What is this repository for? ###

* Quick summary

Repository for development source files for the WHIP (Wound Healing ITeM Project), specifically the LED photostimulation device.

A _Work notes_ document can be found [here (Overleaf)](https://www.overleaf.com/read/jpbcpzvpcswm)

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* Repo owner: [Martín Canziani <martin.canziani (at) hs-furtwangen.de>](mailto:martin.canziani@hs-furtwangen.de)

### Folder structure ###

* Arduino

libraries > libraries for the Arduino sketches

simple-i2c-comm > Sketch for the Arduino to send I2C commands controlled by USB

test-sketches > Tests done during development

* Hardware

arduinoShield > The board that goes on top of the arduino and has the power input, a DC/DC converter and the routing of I2C and power lines into the Ethernet cable

evalAdapter > The board that adapts the Ethernet cable to the Eval Kit inputs pins

humidity > Some resources about electronics on high humidity environments

incubator > Photos and documentation of the incubator

led-board > Altium PCB project for the LED Board

led-board-small > Altium PCB project for the LED Board that was manufactured, with only 16 RGB LEDs

led-driver > documentation on the Evaluation Module

* labview 

Resources on LabView

* led-driver-boards

Comparison of led drivers and evaluation boards to buy

* minutes

Minutes for every meeting I had on ITeM

* old-gantts

GanttProject and PDF files for the succesive updates on the Gantt chart

* optical-measurements

Data, resources and files for optical measurements done

* papers

Papers about the project topics

* publications

Resources, text and references for the Device Paper written for the BMT 2017 conference

